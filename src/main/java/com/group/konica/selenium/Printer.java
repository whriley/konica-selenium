/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group.konica.selenium;

import java.util.HashMap;

/**
 *
 * @author peterr
 */
public class Printer {
    String pnameValue;
    String pModelValue;
    String pHostName;
    String pMacAddress;
    String pIpValue;
    String dValue;
    String usernameValue;
    HashMap <String, String> hmap = new HashMap();
    
   public Printer(String Ip)
    {
        pnameValue = Ip;
        pIpValue = Ip;
    }
    
    public Printer(String HostName,String pModel, String Ip, String MacAddress)
    {
        pHostName = HostName;
        pMacAddress = MacAddress;
        pModelValue = pModel;
        pIpValue = Ip;
    }
    
    public void addScanValue(String username, String destinationValue)
    {
        hmap.put(username, destinationValue);
    }
    public int ScanMapSize()
    {
        return hmap.size();
    }
    public String getIp()
    {
        return pIpValue;
    }
    public HashMap getMap()
    {
        return hmap;
    }
    public void setMap(HashMap ScanVal)
    {
        hmap = ScanVal;
    }
    public String getPrinterModel()
    {
        return pModelValue;
    }
    public String getpHostName()
    {
        return pHostName;
    }
    public void getpHostName(String hName)
    {
        pHostName = hName;
    }
     public String getpMacAddress()
    {
        return pMacAddress;
    }
    public void setpMacAddress(String mac)
    {
        pMacAddress = mac;
    }
}
