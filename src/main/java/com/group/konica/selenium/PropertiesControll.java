package com.group.konica.selenium;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.io.FileInputStream;

/** Properties File kezelő osztály.
 *
 * Ez a fájl tartalmazza
 * A properties file root mappában van. (Amiben a target is van.)
 * 
 * @author Péter Richárd
 */
public class PropertiesControll {

    Properties prop;
    String ChromeDriverPath = "";
    String printerFile = "";
    String ScanCSVFile = "";
    int MaxNo = 0;
    //the base folder is ./, the root of the main.properties file  
    String propFileName = "./konicaSelenium.properties";
    
    FileInputStream inputStream;

    public void PropertiesControll() throws IOException{
        try {
                prop = new Properties();
                //load the file handle for main.properties
                inputStream = new FileInputStream(propFileName);
                if (inputStream != null) {
                        prop.load(inputStream);
                } else {
                        throw new FileNotFoundException("property file '" + propFileName + "' not found");
                       
                }
                // get the property value and print it out
                ChromeDriverPath = prop.getProperty("ChromeDriverPath");
                printerFile = prop.getProperty("printerFile");
                ScanCSVFile = prop.getProperty("ScanCSVFile");
                MaxNo = Integer.parseInt(prop.getProperty("MaxNo"));
        } catch (Exception e) {
                System.out.println("Exception: " + e);
        } finally {
                inputStream.close();
        }
    }
    
    public int getMaxNo() {
        return MaxNo;
    }
    
    public String getChromeDriverPath() {
        return ChromeDriverPath;
    }

    public String getprinterFile() {
        return printerFile;
    }
    
    public String getScanCSVFile() {
        return ScanCSVFile;
    }
    
     public String propFileName()
    {
        return propFileName;
    }
         
/* Mentes helyét kell javítani, de nem használt funkció, ezért abbamaradt 
    
    public void PropertiesWrite(String inputPath, String outputPath) throws IOException {
        try {
            prop = new Properties();
            if (inputStream != null) {
                    prop.load(inputStream);
            } else {
                    throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
            FileOutputStream outputStream = new FileOutputStream(propFileName);
            prop.put("inputPath", inputPath);
            prop.put("defoutputPath", outputPath);
            prop.store(outputStream, "This is a sample properties file");
            outputStream.close();
        }catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
        //Populating the properties file
        //Storing the properties file
        System.out.println("Properties file modified......"+propFileName);
        logger.info("Properties file modified......"+propFileName);
    }
  */  
}
    