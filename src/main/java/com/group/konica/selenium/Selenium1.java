package com.group.konica.selenium;

/**
 * Listát készít a Konica Minolta bizhub típusú nyomtatók Scan címekről.
 * Változók:
 * chromedriver - elérési út
 * plist - Nyomtatók IP (printers.txt helye, soronként olvassa be az IP-t)
 * ScanCSVFile - Kimenő CSV
 * --> Nem találtam megoldás a táblázat utolsó sorának No. szám megkeresésére ezért maxNo változóig nézzük
 * --> Ha nem elérhető a nyomtató az nem kerül az eredmény csvbe! (EZT MÉG MEGOLDANI)
 * @author peterrichard
 */

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.File;    

public class Selenium1 {
    
    public static void main(String[] args) throws InterruptedException 
    {
        String ChromeDriverPath = "";
        String printerFile = "";
        String ScanCSVFile = "";
        int maxNo = 0; // a táblázat feltételezett legnagyobb mérete (Store address)
        try{
            PropertiesControll proper = new PropertiesControll();
            proper.PropertiesControll();
            ChromeDriverPath = proper.getChromeDriverPath();
            printerFile = proper.getprinterFile();
            ScanCSVFile = proper.getScanCSVFile();
            maxNo = proper.getMaxNo();
        }catch(IOException e)
        {
            System.out.println("Properties file read error!: " + e);
        }
        WebDriver driver;
        System.setProperty("webdriver.chrome.driver",ChromeDriverPath);
        driver = new ChromeDriver();
        List plist = new LinkedList();
        HashSet<Printer> p = new HashSet<Printer>();
        String printerModel;
        String printerHostName = "";
        String pMacAddress = "";
        String ScanDestVal; // Aktuális kiolvasott sor 
        String ScanNameVal; // Aktuális kiolvasott sor  
        
        // Printers.txt beolvasása LinkedListbe
        try{
            File file = new File(printerFile); 
            Scanner sc = new Scanner(file); 
            String readLine=null;
            while (sc.hasNextLine())
            {
                readLine = sc.nextLine();   
                if(ipvalidate(readLine))
                {
                    plist.add(readLine);
                }
                else
                {
                    System.out.println("------> Readed line is: "+readLine+" NOT VALID IPv4 Address! DROP");
                }
            } 
        }catch(IOException ex)
        {
            System.out.println(ex);
        }
        // printers.txt ip címek kiolvasása, majd futtatása
        for(int j=0;j<plist.size();j++)
        {
            try{
            driver.get("http://"+plist.get(j));
            driver.manage().window().maximize();    
            driver.get("http://"+plist.get(j)+"/wcd/system_network.xml");
            // Java altatás
            TimeUnit.SECONDS.sleep(4);
            /* Egy Vizsgalt blokk 
            Ebből a részből áll össze egy vizsgált blokk (Element), mert ha nem találja az Elementet a driver
            Exception-t dob (no such element) és nem folytatja a többi element vizsgálattal (Scan mappák), így a következő
            nyomtatóra lép.
            */
            WebElement phostNameWebElement;
            try{
                phostNameWebElement = driver.findElement(By.xpath("//*[@id=\"S_NET\"]/div[2]/table/tbody/tr[5]/td"));
                printerHostName = phostNameWebElement.getText();
                System.out.println("printerHostName = "+printerHostName);
            }
            catch(Exception e)
            { 
                TimeUnit.SECONDS.sleep(4);
                phostNameWebElement = driver.findElement(By.xpath("//*[@id=\"MC_NetworkInfo\"]/div/table/tbody/tr[5]/td"));
                printerHostName = phostNameWebElement.getText();
                System.out.println("MC_NETWORKINFO -> printerHostName = "+printerHostName);       
            }

            WebElement pMacAddressWebElement;
            try{
                pMacAddressWebElement = driver.findElement(By.xpath("//*[@id=\"S_NET\"]/div[2]/table/tbody/tr[4]/td"));
                pMacAddress = pMacAddressWebElement.getText();
                System.out.println("MAC = "+pMacAddress);
            }
            catch(Exception e)
            {
                pMacAddressWebElement = driver.findElement(By.xpath("//*[@id=\"MC_NetworkInfo\"]/div/table/tbody/tr[4]/td"));
                pMacAddress = pMacAddressWebElement.getText();
                System.out.println("MC_NETWORKINFO -> MAC = "+pMacAddress);
            }
            /* ------------- */
            driver.get("http://"+plist.get(j)+"/wcd/abbr.xml");
            TimeUnit.SECONDS.sleep(4);
            WebElement printerModelWebElement = driver.findElement(By.xpath("//*[@class='devicename-layout']"));
            printerModel = printerModelWebElement.getText();
            System.out.println(printerModel);
            
            // Nem tölti be az oldalt elég idő alatt, itt egy új próba, illetve más elérési úttal!
            if(printerModel == null || "".equals(printerModel))
            {
                printerModelWebElement = driver.findElement(By.xpath("//*[@id=\"Top\"]/div[2]"));
                printerModel = printerModelWebElement.getText();
                System.out.println("IF: "+printerModel);
                TimeUnit.SECONDS.sleep(1);
            }
            // Ha üres a PrinterModel nem tudja vágni, és, hogy ne kapjunk ArrayIndexOutOfBoundsException: 1
            if(!(printerModel.isEmpty()))
            {
                printerModel = printerModel.split(":")[1];
            }
            // Scannelési adatok gyűjtése (List objektumba)
            List col = driver.findElements(By.xpath("/html/body/div[4]/div[2]/div/div[2]/form[3]/div/table/tbody/tr/th"));
            System.out.println(plist.get(j) + " - " + printerModel + " - No of cols are : " +col.size()); 
            List rows = driver.findElements(By.xpath(".//*[@id='C_ABR_LI']/div/table/tbody/tr/td[1]")); 
            System.out.println(plist.get(j) + " - " + printerModel + " - No of rows are : " + rows.size());
            /*
                Mivel a nyomtató a számokat (no.) a táblázatban nem mindig sorrendben írja ezért exception lesz!
                C_ABR_H_NAM(1..2..4..5..11) ( <input type="hidden" id="C_ABR_H_NAM1" value="Racz Tibor"> )
                --> Nem találtam megoldás a táblázat utolsó sorának No. szám megkeresésére ezért maxNo változóig nézzük
            */
            // Aktuális (PObj) Printer objektum létrehozása, majd Setbe rakás (p)
            Printer pObj = new Printer(printerHostName,printerModel,plist.get(j).toString(), pMacAddress);
            // Aktuális HashMap készítés amit majd a Printer objektum megkap (Név (Vasaros Livia) + Érték (UVEG002))
            HashMap hMapPrinterValues = new HashMap();
            // végig megyünk a táblázaton
            for(int i=1;i<=maxNo;i++)
                {
                    // default érték, főleg ha exception keletkezik
                    ScanDestVal = "null";
                    ScanNameVal = "null";
                    try{
                        // megkeressük ezeket a C_ABR_H_NAM id-s tageket ezek hiddenesek és tartalmazzák a value-t is
                        // pl: <input type="hidden" id="C_ABR_H_NAM1" value="Racz Tibor">
                        WebElement Scan = driver.findElement(By.id("C_ABR_H_NAM"+i));
                        // System.out.println("Scan:  "+Scan);
                        ScanDestVal = Scan.getAttribute("value");
                        // System.out.println("ScanDestVal: "+ScanDestVal);
                        Scan = driver.findElement(By.id("C_ABR_H_DST"+i));
                        ScanNameVal = Scan.getAttribute("value");
                        // System.out.println("ScanNameVal: "+ScanNameVal);
                        hMapPrinterValues.put(ScanDestVal, ScanNameVal);
                    }
                    // Ne szemetelje a képernyőt így ha nem találjuk az "C_ABR_H_NAM"X"-et akkor csak informál
                    catch(WebDriverException elementEx){
                        System.out.println("Information: "+plist.get(j)+" - C_ABR_H_NAM"+i+" - Not found");
                    }
                    // Ha valami más gond van
                    catch(Exception e){
                        System.out.println(plist.get(j)+" - Exception!: "+printerModel+" - Exception cause: "+e.getMessage());
                        // System.out.println("Values: "+ScanNameVal + " - " + ScanDestVal+" - i:"+i);
                    }
                    finally
                    {
                        // Így bármilyen Catch ágra futás esetén is odaadhato az aktuális Printernek (PObj)
                        pObj.setMap(hMapPrinterValues);
                        p.add(pObj);
                    }
                }
            // Ha a weboldal unknown error: net::ERR_CONNECTION_TIMED_OUT
            }catch(WebDriverException webDriverEx){
                System.out.println(plist.get(j) +" -> Error: "+ webDriverEx.getMessage());
                Printer pObj = new Printer("NA",webDriverEx.getMessage(),plist.get(j).toString(), webDriverEx.getMessage());
                p.add(pObj);
                System.out.println(p.toString());
            // Ha valami más gond van   
            }catch(InterruptedException e){
                System.out.println(e);
            }
        }

        // using iterators and write to csv
            try (FileWriter writer = new FileWriter(ScanCSVFile)) 
            {
                HashMap printerValues = new HashMap();    
                for (Iterator<Printer> it = p.iterator(); it.hasNext(); ) 
                {
                    Printer pakt = it.next();
                    printerValues = pakt.getMap();
                    String eol = System.getProperty("line.separator");
                    Iterator<Map.Entry<String, String>> itr = printerValues.entrySet().iterator(); 
                    while(itr.hasNext()) 
                    { 
                        Map.Entry<String, String> entry = itr.next(); 
                        System.out.println("Printer = "+pakt.getIp() + ", Key = " + entry.getKey() +  
                                             ", Value = " + entry.getValue()); 
                        writer.append(pakt.getIp())
                              .append(',')
                              .append(entry.getKey())
                              .append(',')
                              .append(entry.getValue())
                              .append(',')        
                              .append(pakt.getPrinterModel())
                              .append(',')        
                              .append(pakt.getpHostName())
                              .append(',')        
                              .append(pakt.getpMacAddress())
                              .append(eol);
                    }    
                }
            }catch (IOException ex) {
              ex.printStackTrace(System.err);
            }

        //Closing the driver instance
        driver.close();
        driver.quit();       
    }
    
    /**
* ipvalidate függvény.
* Ip cím ellenőrzés, validálás. Hibás (False) vagy helyes (True)
* 
* @return  Boolean 
* @author  Péter Richárd
* @version 1.0
* @since   2021-09-06
*/
    public static boolean ipvalidate(final String ip) {
        String PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";
        return ip.matches(PATTERN); 
    }
 
    /* Properties file lecserélte
    public static File ChromeDriveChooser() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Select ChromeDriver.exe:");
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        // Csak exe fileok jelenjenek meg
        FileFilter filter = new FileNameExtensionFilter("EXE File","exe");
        fileChooser.setFileFilter(filter);
        int result = fileChooser.showOpenDialog(new JDialog());
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            System.out.println("Selected file: " + selectedFile.getAbsolutePath());
            return selectedFile;
        }
        else
        return null;
    }
    */
    
}


